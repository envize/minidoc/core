import Flow from "./Flow";
import Markdown from "./Markdown";

export default class Document {
    public title: string;
    public story: Markdown | null = null;
    public general: Markdown | null = null;
    public happyFlow: Flow | null = null;
    public alternateFlows: Flow[] = [];

    constructor(title: string) {
        this.title = title;
    }
}
