import IParent from './IParent';
import Document from './Document';
import Breadcrumb from './Breadcrumb';
import Bundle from './Bundle';

const rootIndicator = '/';
const rootDescription = 'root';
const seperator = '/';

export default interface INode {
    readonly scopedId?: string;
    readonly id: string;

    readonly parent?: IParent;
    readonly document: Document | null;
    readonly breadcrumbs: Breadcrumb[];
}

export function getId(node: INode): string {
    const scopedIds: string[] = [];

    if (node instanceof Bundle) {
        return rootIndicator;
    }

    let currentNode: INode | undefined = node;
    while (currentNode !== undefined) {
        if (currentNode.scopedId !== undefined) {
            scopedIds.unshift(currentNode.scopedId);
        }

        currentNode = currentNode.parent;
    }

    const id = scopedIds.join(seperator);

    return id;
}

export function getBreadcrumbs(node: INode): Breadcrumb[] {
    const breadcrumbs: Breadcrumb[] = [];

    let currentNode: INode | undefined = node;
    while (currentNode !== undefined) {
        const id = currentNode.id;
        let description: string | undefined;

        if (currentNode instanceof Bundle) {
            description = rootDescription;
        } else if (currentNode.document !== null) {
            description = currentNode.document.title;
        }

        const breadcrumb = new Breadcrumb(id, description);
        breadcrumbs.unshift(breadcrumb);

        currentNode = currentNode.parent;
    }

    return breadcrumbs;
}


export function toJSON<TNode extends INode>(node: TNode): TNode {
    return Object.assign({}, node, { parent: null });
}
