import INode, { getId, getBreadcrumbs, toJSON } from './INode';
import IParent from './IParent';
import Document from './Document';
import Breadcrumb from './Breadcrumb';

export default class Page implements INode {
    public readonly scopedId: string;
    public readonly id: string;

    public readonly parent?: IParent;
    public readonly breadcrumbs: Breadcrumb[];

    public readonly document: Document | null;

    constructor(
        scopedId: string,
        parent: IParent | undefined,
        document: Document | null) {

        this.scopedId = scopedId;
        this.parent = parent;
        this.document = document;

        this.id = getId(this);
        this.breadcrumbs = getBreadcrumbs(this);
    }

    public toJSON(): Page {
        return toJSON(this);
    }
}
