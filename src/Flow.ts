import FlowType from "./FlowType";
import Link from "./Link";
import Step from "./Step";

export default class Flow {
    public type: FlowType;
    public description: string | null = null;
    public steps: Step[] = [];
    public start: Link | null = null;
    public end: Link | null = null;

    constructor(type: FlowType) {
        this.type = type;
    }
}
