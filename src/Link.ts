import Step from "./Step";

export default class Link {
    public target: Step;
    public description: string | null = null;

    constructor(target: Step) {
        this.target = target;
    }
}
