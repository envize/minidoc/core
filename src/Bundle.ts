import IParent from './IParent';
import INode, { getId, getBreadcrumbs, toJSON } from './INode';
import Breadcrumb from './Breadcrumb';

export default class Bundle implements IParent {
    public readonly scopedId = undefined;
    public readonly id: string;

    public readonly parent = undefined;
    public readonly children: INode[] = [];
    public readonly breadcrumbs: Breadcrumb[];

    public readonly document = null;

    constructor() {
        this.id = getId(this);
        this.breadcrumbs = getBreadcrumbs(this);
    }

    public toJSON(): Bundle {
        return toJSON(this);
    }
}
