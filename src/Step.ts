import StepType from "./StepType";

export default class Step {
    public position: number = 0;
    public id: string | null = null;
    public type: StepType;
    public description: string;

    constructor(type: StepType, description: string) {
        this.type = type;
        this.description = description;
    }
}
