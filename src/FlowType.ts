enum FlowType {
    HappyFlow = 'happy-flow',
    AlternateFlow = 'alternate-flow',
    Exception = 'exception'
}

export default FlowType;
