export default class Breadcrumb {
    constructor(
        public id: string,
        public description?: string
    ) { }
}
