import INode from './INode';

export default interface IParent extends INode {
    readonly children: INode[];
}
